import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Container, Row, Col, Figure } from 'react-bootstrap';
import MyNavbar from './components/MyNavbar';
import MyTable from './components/MyTable';
import RegisterForm from './components/RegisterForm';


export default class App extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
            persons: [
                {
                    username: "Chhaihuo",
                    gender: "male",
                    email: "pengchhaihuo@gmail.com",
                    password: "12345",
                },
                {
                    username: "Sokny",
                    gender: "male",
                    email: "sokny123@gmail.com",
                    password: "54321",
                },
                {
                    username: "parinha",
                    gender: "male",
                    email: "nhatach99@gmail.com",
                    password: "4444",
                },
            ]
        }
    }


    addPerson = (newPerson)=>{
        const temp = [...this.state.persons];
    temp.push(newPerson);
    this.setState({ persons: temp });   
    }

    render() {
        return (
            
            <Container>
                <Row>
                    <Col>
                        <MyNavbar />
                    </Col>
                </Row>
                <Row className="my-5">
                    <div className={"w-"}></div>
                    <Col lg={4} md={4} className="mx-2 px-3 rounded">
                        <Figure className={"p-0 m-0 d-flex justify-content-center"}>
                            <Figure.Image
                                width={80}
                                height={80}
                                alt="Image"
                                src="images/logo.svg"
                            />
                        </Figure>
                        <h2 className={"p-0 m-0 d-flex justify-content-center"}>Create Account</h2>
                        <RegisterForm  addPerson ={this.addPerson }/>
                    </Col>
                    <Col>
                        <MyTable persons={this.state.persons}/>
                    </Col>

                </Row>
            </Container>
        )
    }
}
