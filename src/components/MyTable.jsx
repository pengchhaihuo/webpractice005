import 'bootstrap/dist/css/bootstrap.min.css';
import { Table, Button } from 'react-bootstrap';
import React from 'react'

export default function MyTable(props) {

    const persons = props.persons;

    return (
        <>
        <h2 style={{color:"gray"}}>Table Accounts</h2>
        <Table striped bordered hover>
            <thead>
                <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th colSpan="2">Email</th>
                    <th>Gender</th>
                </tr>
            </thead>
            <tbody>
               {persons.map((item,index)=>
               (
                <tr key={index}>
                <td>{index+1}</td>
                <td>{item.username}</td>
                <td colSpan="2">{item.email}</td>
                <td>{item.gender}</td>
            </tr>
               ))}
            </tbody>
        </Table>
        <Button variant="danger" onSubmit="false" className={"shadow rounded"}>Delete</Button>
        </>
    )
}
