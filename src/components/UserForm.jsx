import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";

export default class UserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
    };
  }

  getEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  getPassword = (e) => {
    this.setState({
      password: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <Form>
          <Form.Group controlId='formBasicEmail'>
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type='email'
              placeholder='Enter email'
              onChange={(e) => this.getEmail(e)}
            />
            <Form.Text className='text-muted'>
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>

          <Form.Group controlId='formBasicPassword'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              type='password'
              placeholder='Password'
              onChange={(e) => this.getPassword(e)}
            />
          </Form.Group>
          <Form.Group controlId='formBasicCheckbox'>
            <Form.Check type='checkbox' label='Check me out' />
          </Form.Group>
          <Button
            variant='primary'
            type='submit'
            onClick={() => this.props.onAdd(this.state)}>
            Submit
          </Button>
        </Form>
      </div>
    );
  }
}
